import sqlite3

class Database():

	conn = None

	def __init__(self):
		try:
			self.conn = sqlite3.connect('data.sqlite3')
			self.conn.row_factory = sqlite3.Row
		except Exception as e:
			raise e

	def __del__(self):
		self.conn.close()
		self.conn = None

	def update_single(self, query, data):
		try:
			c = self.conn.cursor()
			c.execute(query, data)
			self.conn.commit()
			return not c.rowcount == len(data)
		except Exception as e:
			raise e

	def select(self, query, data=()):
		try:
			c = self.conn.cursor()
			c.execute(query, data)
			return c.fetchall()
		except Exception as e:
			raise e

	def insert_single(self, query, data):
		try:
			c = self.conn.cursor()
			c.execute(query, data)
			self.conn.commit()
			return not c.rowcount == len(data)
		except Exception as e:
			raise e

	def insert_many(self, query, data):
		try:
			c = self.conn.cursor()
			c.executemany(query, data)
			self.conn.commit()
			return not c.rowcount == len(data)
		except Exception as e:
			raise e

	def delete_single(self, query, data):
		try:
			c = self.conn.cursor()
			c.execute(query, data)
			self.conn.commit()
			return not c.rowcount == len(data)
		except Exception as e:
			raise e


