import paramiko
import io
import socket

class ConnectionManager:

	def __init__(self, db):
		self.db = db

	def connect_to_router(self, router_id):
		details = self.get_router_connection_details(router_id)
		if(details["connection_type"]=="ssh"):
			return self.connect_ssh(details)
		#elif(details["connection_type"]=="telnet"):
		#	return self.connect_telnet(details)
		else:
			raise ValueError("Connection type \"{}\" on connection ID {} is not valid".format(details["connection_type"], details["connection_id"]))

	def connect_ssh(self, connection_details):
		connection = None
		if("jump_host" in connection_details):
			if(connection_details["jump_host"]["connection_type"]=="ssh"):
				connection = self.connect_ssh(connection_details["jump_host"])
			elif(connection_details["jump_host"]["connection_type"]=="telnet"):
				connection = self.connect_telnet(connection_details["jump_host"])
			else:
				raise ValueError("Connection type \"{}\" on connection ID {} is not valid".format(details["connection_type"], details["connection_id"]))
		ssh_connection=paramiko.SSHClient()
		ssh_connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())

		if(connection): #https://stackoverflow.com/questions/35304525/nested-ssh-using-python-paramiko
			sshtransport = connection.get_transport()
			dest_addr = (connection_details["host"], int(connection_details["port"])) 
			local_addr = ('127.0.0.1', 22)
			sshconnection = sshtransport.open_channel("direct-tcpip", dest_addr, local_addr)
		if(connection_details["auth_type"]=="password"):
			if(connection):
				ssh_connection.connect(hostname=connection_details["host"],username=connection_details["username"],password=connection_details["password"], sock=sshconnection)
			else:
				ssh_connection.connect(hostname=connection_details["host"],username=connection_details["username"],password=connection_details["password"])
		elif(connection_details["auth_type"]=="private_key"):
			key = paramiko.RSAKey(file_obj=io.StringIO(connection_details["private_key"]))	
			if(connection):
				ssh_connection.connect(hostname=connection_details["host"],username=connection_details["username"],password=connection_details["password"], pkey=key, sock=sshconnection)
			else:
				ssh_connection.connect(hostname=connection_details["host"],username=connection_details["username"],password=connection_details["password"], pkey=key)
		else:
			raise ValueError("Invalid Authentication type for connection ID {}".format(connection_details["connection_id"]))
		return ssh_connection
			
	def send_ssh_command(self, connection, command):
		#raise NotImplementedError("Send SSH Command") 
		if(connection):
			stdin, stdout, stderr = connection.exec_command(command)
			return stdout.read().decode()
		else:
			raise ValueError("Send SSH Command, no connection")
	
	## Add error handling for router not existing
	def get_router_connection_details(self, router_id):
		router_conn_details = self.db.select("SELECT connection.connection_id, connection.host, connection.port, connection.auth_type, connection.username, connection.password, connection.private_key, connection.connection_type, connection.jump_host FROM connection LEFT JOIN router ON router.connection_id=connection.connection_id WHERE router.router_id=?", (router_id,))
		for conn in router_conn_details:
			if(conn["jump_host"]):
				jump_host = self.get_connection_details(conn["jump_host"])
				return {"connection_id": conn["connection_id"], "host": conn["host"], "port": conn["port"], "auth_type": conn["auth_type"], "username": conn["username"], "password": conn["password"], "private_key": conn["private_key"], "connection_type": conn["connection_type"], "jump_host": jump_host}
			else:
				return {"connection_id": conn["connection_id"], "host": conn["host"], "port": conn["port"], "auth_type": conn["auth_type"], "username": conn["username"], "password": conn["password"], "private_key": conn["private_key"], "connection_type": conn["connection_type"]}

	## Add error handling for connection not existing
	def get_connection_details(self, connection_id):
		conn_details = self.db.select("SELECT connection.host, connection.port, connection.auth_type, connection.username, connection.password, connection.private_key, connection.connection_type, connection.jump_host FROM connection WHERE connection.connection_id=?", (connection_id,))
		for conn in conn_details:
			if(conn["jump_host"]):
				jump_host = self.get_connection_details(conn["jump_host"])
				return {"connection_id": connection_id, "host": conn["host"], "port": conn["port"], "auth_type": conn["auth_type"], "username": conn["username"], "password": conn["password"], "private_key": conn["private_key"], "connection_type": conn["connection_type"], "jump_host": jump_host}
			else:
				return {"connection_id": connection_id, "host": conn["host"], "port": conn["port"], "auth_type": conn["auth_type"], "username": conn["username"], "password": conn["password"], "private_key": conn["private_key"], "connection_type": conn["connection_type"]}

