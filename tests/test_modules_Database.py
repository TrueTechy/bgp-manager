from modules.Database import Database
from modules.MediaManager import MediaManager

db = Database()

def test_db_connection():
	assert(db)

def test_db_select():
	assert(db.select("SELECT * FROM user WHERE UID=1")[0]["username"]) == "Abby"

def test_db_insert_single():
	db.insert_single("INSERT INTO `user` (username) VALUES (?)", ('automatedTest',))
	assert(db.select("SELECT * FROM user WHERE `username`=?", ("automatedTest",))[0]["username"]) == "automatedTest"
	db.delete_single("DELETE FROM `user` WHERE `username`=?", ("automatedTest",))

def test_db_insert_many():
	db.insert_many("INSERT INTO `user` (username) VALUES (?)", (("automatedTest1",), ("automatedTest2",)))
	assert(db.select("SELECT * FROM user WHERE `username`=?", ("automatedTest1",))[0]["username"]) == "automatedTest1"
	assert(db.select("SELECT * FROM user WHERE `username`=?", ("automatedTest2",))[0]["username"]) == "automatedTest2"
	db.delete_single("DELETE FROM `user` WHERE `username`=?", ("automatedTest1",))
	db.delete_single("DELETE FROM `user` WHERE `username`=?", ("automatedTest2",))

def test_db_update_single():
	db.insert_single("INSERT INTO `user` (username) VALUES (?)", ('automatedTest',))
	assert(db.select("SELECT * FROM user WHERE `username`=?", ("automatedTest",))[0]["username"]) == "automatedTest"
	db.update_single("UPDATE user SET username='automatedTest2' WHERE `username`=?", ("automatedTest",))
	assert(db.select("SELECT * FROM user WHERE `username`=?", ("automatedTest2",))[0]["username"]) == "automatedTest2"
	db.delete_single("DELETE FROM `user` WHERE `username`=?", ("automatedTest2",))

def test_delete_single():
	db.insert_single("INSERT INTO `user` (username) VALUES (?)", ('automatedTest',))
	db.delete_single("DELETE FROM `user` WHERE `username`=?", ("automatedTest",))
	assert(db.select("SELECT * FROM user WHERE `username`=?", ("automatedTest",))) == []
